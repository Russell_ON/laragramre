<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];

    public function profileImage(){
        $imagePath = $this->image ?? 'profile/p4BJIvVdw6BdfqFQ1WOsBp6F56ieElegLKQE9nGS.png';
        return '/storage/' . $imagePath;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function followers(){
        return $this->belongsToMany(User::class);
    }

    public function comments(){
        return $this->belongsToMany(Comment::class);
    }
}
