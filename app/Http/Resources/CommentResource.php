<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'comment_id' => $this->id,
            'description' => $this->description,
            'profile_title' => $this->profile->title,
            'profile_image' => $this->profile->image,
            'profile_id' => $this->profile->id,
        ];
    }
}
