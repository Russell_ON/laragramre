<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{

    public function allUsers(){

        // $users = User::all(function($user){
        //     return $user->id !== Auth::user()->id;
        // });

        $users = User::all()->except(Auth::id());

        return view('profiles.all', ['users' => $users]);
    }

    public function index(User $user){
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;

        return view('profiles.index',  compact('user' , 'follows'));
    }

    public function edit(User $user){

        $this->authorize('update', $user->profile);

        return view('profiles.edit', compact('user'));
    }

    public function update(User $user){

        $this->authorize('update', $user->profile);

        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'url',
            'image' => '',
        ]);

        if(request('image')) {

            $imagePath = (request('image')->store('profile', 'public'));

            $image = Image::make(public_path("storage/$imagePath"))->fit(600,600);
            $image->save();

            $imageArray = ['image' => $imagePath];
        }


        $user->profile->update(array_merge(
            $data,
            $imageArray ?? []
        ));

        return redirect('/profile/'. auth()->user()->id);

    }

    public function search(Request $request)
    {
        // $users_q = User::query();
        $users_q = User::with('profile');
        $search = $request->get('search');

        if($search !== null){
            $users_q->where('username', 'like', '%' . $search . '%');
        }

        $users = $users_q->get();
        return UserResource::collection($users);
    }
}
