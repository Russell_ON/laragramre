<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Resources\CommentResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Post $post){

        // dd($post->comments);
        $data = request()->validate([
            'description' => 'required',
            'profile_id' => 'required',
        ]);
        // dd($data);

        // dd(Auth::user()->profile());
        $comment = $post->comments()->create([
            'description' => $data['description'],
            'profile_id' => $data['profile_id'],

        ]);

        return redirect()
        ->back()
        ->withStatus('Comment was created!');
    }

    public function delete(Comment $comment){
        $comment->delete();
        return redirect()->back();
    }

    public function load_comments(Request $req, $post){
        $skip = $req->get('skip');
        $comments = Comment::where('post_id',$post)
        ->latest()
        ->skip($skip)
        ->take(3)
        ->get();
        // $commentsJSON = $comments->toJson();
        return CommentResource::collection($comments);
    }
}
