<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     protected $fillable = ['description', 'profile_id'];

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function profile(){
        return $this->belongsTo(Profile::class);
    }
}
