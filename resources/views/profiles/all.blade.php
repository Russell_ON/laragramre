@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($users as $user)
    <div class="row">
        <div class="col-sm-md-3 p-5">
            <img src="{{$user->profile->profileImage()}}" height="150px" width="150px" class="rounded-circle">
        </div>

        <div class="col-sm-md-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                    <a href="/profile/{{$user->id}}"> <h1>{{$user->username}}</h1></a>
                <follow-button user-id = "{{$user->id}}" follows = "{{$follows ?? ''}}"></follow-button>
            </div>

            <div class="d-flex">
                <div class="pr-5"><strong>{{ $user->posts->count() }}</strong> posts</div>
                <div class="pr-5"><strong>{{$user->profile->followers->count()}}</strong> follower</div>
                <div class="pr-5"><strong>{{$user->following->count()}}</strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{$user->profile->description}}</div>
            <div><a href="#">{{$user->profile->url ?? 'N/A'}}</a></div>
        </div>
    </div>
    @endforeach

    <a href="{{ route('feed') }}"><button type="button" class="btn btn-primary">Go to your feed!</button></a>
</div>
@endsection
