@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3 p-5">
            <img src="{{$user->profile->profileImage()}}" height="150px" width="150px" class="rounded-circle">
        </div>

        <div class="col-md-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>{{$user->username}}</h1>
                <follow-button user-id = "{{$user->id}}" follows = "{{$follows}}"></follow-button>
                @can('update', $user->profile)
                <a href="{{route('post.create')}}">Add new post</a>
                @endcan
            </div>

            @can('update', $user->profile)
            <a href="{{route('profile.edit', ['user' => $user->id ])}}">Edit profile</a>
            @endcan

            <div class="d-flex">
                <div class="pr-5"><strong>{{ $user->posts->count() }}</strong> posts</div>
            <div class="pr-5"><strong>{{$user->profile->followers->count()}}</strong> follower</div>
            <div class="pr-5"><strong>{{$user->following->count()}}</strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{$user->profile->description}}</div>
            <div><a href="{{$user->profile->url ?? '#'}}">{{$user->profile->url ?? 'N/A'}}</a></div>
        </div>
    </div>

    <div class="row pt-5">
        @foreach($user->posts as $post)
        <div class="col-sm-4 pb-4 px-4">
           <a href="/p/{{ $post->id }}">
               <img src="/storage/{{$post->image}}" class="w-100">
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
