@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <img class="img-fluid" src= "/storage/{{$post->image}} " alt="w-100" height="600px" width="600px">
        </div>
        <div class="col-md-4">
            <div>
                <div class="d-flex align-items-center pt-4">
                    <div class="pr-3">
                        <img src= "{{$post->user->profile->profileImage()}} " class="rounded-circle w-100" style="max-width: 40px;">
                    </div>
                    <div>
                        <div class="font-weight-bold"><a href="/profile/{{$post->user->id}}"><span class="text-dark">{{$post->user->username}}</span></a> @can('update', $post->user->profile)<a href="/p/{{ $post->id }}/delete">Delete Post</a>@endcan </div>
                    </div>
                </div>

                <hr>

                <p><a href="/profile/{{$post->user->id}}"><span class="font-weight-bold text-dark">{{$post->user->username}}</span></a> {{$post->caption}}</p>

            </div>

        </div>
    </div>
</div>
@endsection('content')
