@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($posts as $post)
    <div class="row">
        <div class="col-sm-8 pt-5">
            <img id="post-id-{{ $post->id }}" class="img-fluid" src= "/storage/{{$post->image}} " alt="w-100">
        </div>

        <div class="col-sm-4 pt-5">
            <div>
                <div class="d-flex align-items-center">
                    <div class="pr-3">
                        <img src= "{{$post->user->profile->profileImage()}} " class="rounded-circle w-100" style="max-width: 40px;">
                    </div>
                    <div>
                        <div class="font-weight-bold"><a href="/profile/{{$post->user->id}}"><span class="text-dark">{{$post->user->username}}</span></a></div>
                    </div>
                </div>

                <hr>
                <p><a href="/profile/{{$post->user->id}}"><span class="font-weight-bold text-dark">{{$post->user->username}}</span></a> {{$post->caption}} <hide-comment post-id= "{{ $post->id}}"></hide-comment></p>
                <comments-section
                    post-id = "{{$post->id}}"
                    csrf="{{ csrf_token() }}"
                    commenter-name="{{auth()->user()->profile->title}}"
                    profile-id="{{auth()->user()->id}}"
                ></comments-section>



            </div>




        </div>
    </div>

    @endforeach
</div>
@endsection('content')
