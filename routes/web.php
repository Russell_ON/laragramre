<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\NewUserWelcomeMail;


Route::get('/', 'PostsController@index')->name('feed');

Auth::routes();

Route::post('/follow/{user}', 'FollowsController@store');
Route::get('/email', function(){
    return new NewUserWelcomeMail();
});

Route::get('/mustfollow/', 'ProfilesController@allUsers');

Route::get('/profile/{user}', 'ProfilesController@index')->name('profile');
Route::get('/profile/{user}/edit', 'ProfilesController@edit')->name('profile.edit');
Route::get('/p/create', 'PostsController@create')->name('post.create');
Route::get('/p/{post}', 'PostsController@show');
Route::get('/p/{post}/delete', 'PostsController@delete')->name('post.delete');
Route::post('/comment/{post}', 'CommentController@store')->name('comment.store');
Route::post('/p', 'PostsController@store')->name('store.post');
Route::patch('/profile/{user}', 'ProfilesController@update')->name('profile.update');
Route::delete('/comment/{comment}/delete', 'CommentController@delete')->name('comment.delete');
Route::get('/comments/{post}', 'CommentController@load_comments')->name('load.comments');
// Route::resource('posts', 'PostController');
